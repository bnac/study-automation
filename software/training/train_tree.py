#!/bin/env python

import optparse
import pickle
import numpy
import sklearn
import sklearn.datasets
import sklearn.cross_validation
import sklearn.ensemble
import sklearn.metrics


usage = "usage: %prog [options] numpy-feature-vector output-name"
parser = optparse.OptionParser(usage=usage)
parser.add_option("-v", "--verbose", dest="verbose",
        action="store_true", default=False, help="Increase verbosity")
parser.add_option("-d", "--decimate", dest="decimate", type="float",
        help="Decimate non-lesion voxels to X%")
parser.add_option("-s", "--subset", dest="subset", type="int",
        help="Use a subset of n rows.")
parser.add_option("-b", "--bagging-pct", dest="bagpct", type="float",
        help="Percent of cases per tree", default=0.25)

(options, args) = parser.parse_args()

if len(args) != 2:
    print "You must supply a feature vector and an output name."
    exit()

output = args[1]

print "Loading data...",
data = numpy.load(args[0])
print "done."

print "Data shape: {}".format(data.shape)

m = data.shape[0]
features = data.shape[-1] - 1 # Last is class
X = data[:,range(features)]
Y = data[:,(features)]

#classifier = sklearn.ensemble.ExtraTreesClassifier(n_estimators=10, max_depth=20,
#	max_features="sqrt", random_state=1, verbose=1, n_jobs=-1)

classifier = sklearn.ensemble.RandomForestClassifier(n_estimators=20, max_depth=20,
	max_features="sqrt", random_state=1, verbose=1, n_jobs=-1)


rand = numpy.random.permutation(m)

train_frac = 0.8

#split_point = int(m * 0.8)

split_point = 500000


print "Training on ", split_point, " samples with ", features, " features."
print "Testing on ", m - split_point, " samples."

Xrt = X[rand[0:split_point]]
Yrt = Y[rand[0:split_point]]

Xrv = X[rand[split_point:-1]]
Yrv = Y[rand[split_point:-1]]

print "Training now..."
classifier.fit(Xrt,Yrt)

preds = classifier.predict(Xrv)
print sklearn.metrics.classification_report(Yrv, preds)
print sklearn.metrics.confusion_matrix(Yrv, preds)
print sklearn.metrics.zero_one_score(Yrv, preds)

#terrs = numpy.zeros(50)
#cerrs = numpy.zeros(50)
#for i in range(1,500000,10000):
#  classifier.fit(Xrt[0:i],Yrt[0:i])
#  terrs[i/10000] = 1 - sklearn.metrics.f1_score(Yrt[0:i], classifier.predict(Xrt[0:i]))
#  cerrs[i/10000] = 1 - sklearn.metrics.f1_score(Yrv, classifier.predict(Xrv))
#print terrs
#print cerrs

pickle.dump(classifier,open(args[1],'wb'))


