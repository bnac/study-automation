#!/bin/bash

slide_maker "flair.nii.gz=flair target_t2.nii.gz=oprois t2_lesions.roi=lesions (flair, 'Raw FLAIR') (flair mask oprois, 'Operator ROIs') (flair mask lesions, 'Automated ROIs') cropfrom=flair"
 
