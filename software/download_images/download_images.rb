#!/bin/env ruby

require 'rubygems'
require 'trollop'
require 'image_formats'
require 'bnac_utils'
require 'yaml'
require 'terminal-table/import'
require 'pp'
require 'yaml'


def colorize(text, color_code)
  "\033[#{color_code}m#{text}\033[0m"
end

def red(text); colorize(text, "31"); end
def green(text); colorize(text, "32"); end
def yellow(text); colorize(text, "33"); end


opts = Trollop::options do
  opt :instance, "Bluesky instance ID", :type => String, :short => 'i'
  opt :examid, "Exam ID to retrieve", :type => String, :short => 'e'
  opt :specfile, "Specification file", :type => String, :short => 's'
end

Trollop::die :instance, "must be specified" unless opts[:instance]
Trollop::die :examid, "must be specified" unless opts[:examid]
Trollop::die :specfile, "must be specified" unless opts[:specfile]

output = `bluesky_info -i #{opts[:instance]} -o #{opts[:examid]} -y`
data = YAML.load(output)

require opts[:specfile]

needed = NEEDED

mris = []
i = 0
data[:mris].each do |k,v|
  mris[i] = [k,v,""]
  i = i + 1
end

mris = mris.sort_by{|m| m[1]}


targets = []

needed.each_with_index do |need, i|

  mris = mris.sort_by{|m| "#{m[2]}-#{m[1]}"}

  target = need[0]

  suggested = nil
  if need[1]
    if need[1].class == Proc
      matched = mris.detect{|m| need[1].call(m[0])}
    else
      matched = mris.detect{|m| need[1].match(m[0])}
    end
    if matched
      suggested = mris.index(matched)
    end
  end

  t = table ["Num", "ID", "", "Series description", "Assigned"]
  mris.each_with_index do |mri,j|
    if suggested and j == suggested
      tag = "*"
    else
      tag = " "
    end
    t << [j+1, mri[1], tag, mri[0], mri[2]]
  end

  begin
    stext = suggested ? " (#{suggested + 1})" : ""
    puts
    puts t
    print "Please select the image for " + green(target) + "#{stext}: "
    resp = gets.chomp
    if resp == "" and suggested
      num = suggested
    else
      num = resp.to_i - 1
    end
    unless num >= 0 and num < mris.size
      puts red("Please select a valid number.")
      raise RuntimeError.new()
    end
    targets[i] = [mris[num][1], target]
    mris[num][2] = target
  rescue
    retry
  end
end

puts
puts green("Final selections:")
t = table ["Num", "ID", "Series description", "Assigned"]
mris.each_with_index do |mri,j|
  t << [j+1, mri[1], mri[0], mri[2]] unless mri[2] == ""
end
puts t

File.open("mri_ids.txt", "w") {|f| f.puts t}

puts 

puts "Downloading scans..."

STDOUT.sync = true 
targets.each do |id, target|
  print "Getting #{target}... "
  defname = `bluesky_retriever -i #{opts[:instance]} -o #{id}`.chomp
  `mv #{defname} #{target}.nii.gz`
  puts "done."
end

puts 
puts "Got all required scans from exam #{opts[:examid]}"
