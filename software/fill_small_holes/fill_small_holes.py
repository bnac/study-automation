#!/bin/env python

import optparse
import numpy
import nibabel
import scipy.ndimage


usage = "usage: %prog [options] input output"
parser = optparse.OptionParser(usage=usage)
parser.add_option("-v", "--verbose", dest="verbose",
        action="store_true", default=False, help="Increase verbosity")
parser.add_option("-t", "--thresh", dest="thresh", type="float",
        help="Set volume threshold in mm3 (only fill <)", default=10)
parser.add_option("-s", "--slices", dest="slices", 
	action="store_true", default=False, 
        help="Process slice-at-a-time (2D).")
(options, args) = parser.parse_args()

img = nibabel.load(args[0])

zooms = img.get_header().get_zooms()
voxel_size = zooms[0] * zooms[1] * zooms[2]


idata = img.get_data()
idata[idata > 0] = 1

if options.slices:
  for s in range(idata.shape[2]):
    islice = idata[:,:,s]
    filled = scipy.ndimage.binary_fill_holes(islice)
    plugs = filled - islice
    (labeled, num) = scipy.ndimage.label(plugs)
    vols = scipy.ndimage.sum(plugs, labeled, range(1, num + 1))
    scaled_vols = vols * voxel_size
    for i in range(1, num + 1):
      if scaled_vols[i - 1] > options.thresh:
        plugs[labeled == i] = 0
    idata[:,:,s] = islice + plugs


else:
  filled = scipy.ndimage.binary_fill_holes(idata)
  plugs = filled - idata
  (labeled, num) = scipy.ndimage.label(plugs)
  vols = scipy.ndimage.sum(plugs, labeled, range(1, num + 1))
  scaled_vols = vols * voxel_size
  for i in range(1, num + 1):
    if scaled_vols[i - 1] > options.thresh:
      plugs[labeled == i] = 0
  idata = idata + plugs


oimg = nibabel.Nifti1Image(idata, img.get_affine())
nibabel.save(oimg, args[1])

